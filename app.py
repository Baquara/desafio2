# -*- coding: utf-8 -*-
from flask import Flask, render_template, request, url_for, flash, redirect, session, jsonify, make_response
import json
import re
import requests
from sqlalchemy import *


db = create_engine('sqlite:///banco.db')
db.echo = False
metadata = MetaData(db)

app = Flask(__name__)


usouweb = False



#Tabelas que serão usadas pelo sqlalchemy
voluntario = Table('voluntario', metadata,
    Column('id', Integer),
    Column('nome', String),
    Column('sobrenome', String),
    Column('bairro', String),
    Column('cidade', String),
    Column('nomecompleto', String),
)


acoes = Table('acoes', metadata,
    Column('id', Integer),
    Column('nome', String),
    Column('instituicao', String),
    Column('local', String),
    Column('descricao', String),
)


@app.route('/')
def index():
    return render_template('main.html')


@app.route('/sobremim')
def sobremim():
    return render_template('sobremim.html')


@app.route('/api/v1/cadvol', methods=('GET', 'POST'))
def cadvol():
    if request.method == 'POST':
        #Recebe o JSON presente no POST:
        dados0 = request.get_json()
        print(dados0)
        #verificando se foi inicializado na web...
        global usouweb
        if(usouweb==False):
            print("Aparentemente o usuário está em CLI...")
        #certificando que o json poderá ser carregando tanto pela versão web como CLI    
        try:
            dados = json.loads(dados0)
        except:
            dados = json.loads(json.dumps(dados0))
        resposta = {"aceito": "true"}
        #se o usuário assinalou o checkbox de introduzir dados no banco...
        vercheckbox = False
        try:
            print(dados["chkbox"]=="on")
            vercheckbox = True
        except:
            vercheckbox = False
        if (vercheckbox==True) or (usouweb==False):
            i = voluntario.insert()
            i.execute(nome=dados["nome"], sobrenome=dados["sobrenome"], bairro=dados["bairro"], cidade=dados["cidade"], nomecompleto=dados["nome"]+" "+dados["sobrenome"])
        else:
            print("Não irá salvar os dados no banco...")
        #retorna o objeto json
        return resposta


@app.route('/api/v1/cadacoes', methods=('GET', 'POST'))
def cadacoes():
    if request.method == 'POST':
        #Recebe o JSON presente no POST:
        dados0 = request.get_json()
        print(dados0)
        #verificando se foi inicializado na web...
        global usouweb
        if(usouweb==False):
            print("Aparentemente o usuário está em CLI...")
        #certificando que o json poderá ser carregando tanto pela versão web como CLI    
        try:
            dados = json.loads(dados0)
        except:
            dados = json.loads(json.dumps(dados0))
        resposta = {"aceito": "true"}
        #se o usuário assinalou o checkbox de introduzir dados no banco...
        vercheckbox = False
        try:
            print(dados["chkbox"]=="on")
            vercheckbox = True
        except:
            vercheckbox = False
        if (vercheckbox==True) or (usouweb==False):
            i = acoes.insert()
            i.execute(nome=dados["nome"], instituicao=dados["instituicao"], local=dados["local"], descricao=dados["descricao"])
        else:
            print("Não irá salvar os dados no banco...")
        #retorna o objeto json
        return resposta


@app.route('/api/v1/voluntario', methods=('GET', 'POST'))
def voluntarioc():
    if request.method == 'GET':
        global usouweb
        usouweb=True
        return render_template('cadvol.html')
    if request.method == 'POST':
        #coleta os resultados como json:
        objson = request.form.to_dict()
        app_json = json.dumps(objson)
        #Envia para a rota designada, que deve fazer a verificação dos dados
        response = requests.post(request.url_root + url_for("cadvol"),json=app_json)
        #Retorna o resultado da rota:
        return (response.json())


@app.route('/api/v1/acoes', methods=('GET', 'POST'))
def acoesc():
    if request.method == 'GET':
        global usouweb
        usouweb=True
        return render_template('cadacoes.html')
    if request.method == 'POST':
        #coleta os resultados como json:
        objson = request.form.to_dict()
        app_json = json.dumps(objson)
        #Envia para a rota designada, que deve fazer a verificação dos dados
        response = requests.post(request.url_root + url_for("cadacoes"),json=app_json)
        #Retorna o resultado da rota:
        return (response.json())
    


#esta rota foi criada apenas para a view onde será visualizadas as Ações.
@app.route('/api/v1/veracoes/', methods=('GET', 'POST'))
def veracoes():
    s = acoes.select()
    rs = s.execute()
    elementos = rs.fetchall()
    if request.method == 'POST':
        objson = jsonify(request.form)
        dados = objson.json
        return redirect(url_for('listing2')+ "?nome=" +dados["nome"])
    return render_template('veracoes.html',elementos=elementos)


#esta rota foi criada apenas para a view onde será visualizados os Voluntários.
@app.route('/api/v1/vervol/', methods=('GET', 'POST'))
def vervol():
    s = voluntario.select()
    rs = s.execute()
    elementos = rs.fetchall()
    if request.method == 'POST':
        objson = jsonify(request.form)
        dados = objson.json
        return redirect(url_for('listing')+ "?nome=" +dados["nome"])
    return render_template('vervol.html',elementos=elementos)


#Endpoint em GET que recebe nome, e retorna a lista de Ações
@app.route('/api/v1/getvol', methods=('GET', 'POST'))
def listing():
    nome = request.args.get('nome', None)
    print(nome)
    try:
        s = select([voluntario]).where(voluntario.columns.nomecompleto.like(nome))
        rs = s.execute()
        row = rs.fetchone()
        print(row)
        d = dict(row.items())
        return jsonify(d)
    except Exception as e:
        print(e)
        return "Não foi possível encontrar resultados para esta requisição."


#Endpoint em GET que recebe nome, e retorna a lista de Ações
@app.route('/api/v1/getacoes', methods=('GET', 'POST'))
def listing2():
    nome = request.args.get('nome', None)
    print(nome)
    try:
        s = select([acoes]).where(acoes.columns.nome==nome)
        rs = s.execute()
        row = rs.fetchone()
        print(row)
        d = dict(row.items())
        return jsonify(d)
    except Exception as e:
        print(e)
        return "Não foi possível encontrar resultados para esta requisição."