# Desafio

Repositório com a solução de um desafio para uma vaga de Desenvolvedor.

Esta solução inclui uma interface gráfica web que pode ser aberta no navegador. Alternativamente, o backend também aceita requisições em CLI.



#### Dependências

- Python 3
- Flask
- sqlalchemy

#### Como executar

>export FLASK_APP=app.py

>flask run

#### Rotas

Cadastrar voluntário:

> /api/v1/cadvol

Cadastrar ações:

> /api/v1/cadacoes

Visualizar informações do voluntário (precisa do nome completo do voluntário):

> /api/v1/getvol?nome=

Visualizar informações da ação (precisa do nome completo da ação):

> /api/v1/getacoes?nome=


#### Exemplos de requisições aceitas pela api

Cadastrar um voluntário:

> curl -X POST -H "Content-Type: application/json"  -d '{"bairro":"Ondina","cidade":"Salvador","nome":"Joao","sobrenome":"Silva"}' http://127.0.0.1:5000/api/v1/cadvol

Cadastrar uma ação:

> curl -X POST -H "Content-Type: application/json"  -d '{"descricao":"Ação humanitária organizada por uma ONG","instituicao":"Instituição Humanitária","local":"Brasil","nome":"Ação Humanitária"}' http://127.0.0.1:5000/api/v1/cadacoes

Visualizar voluntário:

> http://127.0.0.1:5000/api/v1/getvol?nome=Joao%20Silva

Visualizar ação:

> http://127.0.0.1:5000/api/v1/getacoes?nome=A%C3%A7%C3%A3o%20Humanit%C3%A1ria